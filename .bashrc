# /etc/skel/.bashrc
if [[ $- != *i* ]] ; then
    # Shell is non-interactive.  Be done now!
    return
fi

shopt -s checkwinsize

# Put your fun stuff here.
FILE=/usr/local/shared/bash_profile ; [ -f $FILE ] && . $FILE

export PATH=$PATH:$HOME/bin:/usr/sbin:/sbin:$HOME/local/bin
export LESS=' -R '

GREEN_PROMPT='\[\033[1;32m\]'
LIGHTBLUE_PROMPT='\[\033[1;36m\]'
CYAN_PROMPT='\[\033[0;36m\]'

HOST_PROMPT="${GREEN_PROMPT}\h"
USER_PROMPT="${LIGHTBLUE_PROMPT}\u"
WHOLE_PROMPT="${USER_PROMPT}@${HOST_PROMPT}"
#Include all of our bash files, if they exist.
BASH_INCLUDES=("$HOME/.bash_aliases"
"$HOME/engagement/engagementVariables.sh"
"$HOME/.bash_variables"
"$HOME/.bash_localbashfunctions"
)
for CURRENT_INCLUDE in "${BASH_INCLUDES[@]}"; do
    test -f ${CURRENT_INCLUDE} && . $CURRENT_INCLUDE
done

function getBranchForStatus() {
    getBranch &> /dev/null
    if [ "$CURRENT_BRANCH" != "" ] ; then
        echo "(${CURRENT_BRANCH})"
    fi
}

function getBranch() {
    export CURRENT_BRANCH=`git rev-parse --abbrev-ref HEAD`
    echo $CURRENT_BRANCH
}

export PS1="\n\! ${CYAN_PROMPT}\D{%F %T} $WHOLE_PROMPT\[\033[0m\] \[\033[1;33m\]\w ${GREEN_PROMPT}\$(getBranchForStatus)\[\033[0m\]\n\[\033[0m\]$ "
export AWS_PROFILE=saml

alias ls="ls --color"
git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"