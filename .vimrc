filetype plugin indent on

set nocompatible
set et
set sw=4 ts=4 bs=2 fo=cqrt ls=2 ww=<,>,h,l shm=at
set listchars=tab:»·,trail:· " Diplays spaces and tabs with special chars
set selectmode=mouse
set viminfo=%,'50,\"100,:100,n~/.viminfo
set ruler " Displays Row/Column Ruler
set showmatch " Diplays the matching brace when a pair is made
set ai " set Autoindenting on
set nu " set line numbers on
set nowrap " set no line wrapping
set foldmethod=marker
set nohls
set term=linux
set softtabstop=4
set autochdir    " Change directory to the current buffer when opening files
set hlsearch     " Highlights search terms
set incsearch    " Incremential search
set scrolloff=20 " Gives a 20 line buffer when scrolling
set cul          " Highlights the current line
set ignorecase   " Ignores casing
set smartcase    " Notices casing when there's an upper and lower-case
set term=xterm
set complete-=k complete+=k
set foldmethod=indent   "fold based on indent
set foldnestmax=10      "deepest fold is 10 levels
set foldlevel=1         "this is just what i use
set laststatus=2
set t_Co=256
set statusline=
set statusline+=%-10.3n\
set statusline+=%f\
set statusline+=%h%m%r%w
set statusline+=\[%{strlen(&ft)?&ft:'none'}]
set statusline+=%=
set statusline+=0x%-8B
set statusline+=%-14(%l,%c%V%)
set statusline+=%<%P
if v:version > 702
    set relativenumber
    set number
endif
color elflord
if &diff
    set shm+=ToO
    "color desert256
    "color industry
    color slate
endif

hi User1 term=inverse,bold cterm=inverse,bold ctermfg=Green

syn on
behave xterm
noremap <F1> <Esc>
noremap! <F1> <Esc>

nnoremap ; :
nnoremap <CR> :noh<CR><CR>

if !exists('*Scratch')
    function! Scratch()
        :new
        :setlocal buftype=nofile
        :setlocal bufhidden=hide
        :setlocal noswapfile
    endfunction
endif
